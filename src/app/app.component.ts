import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import SignaturePad from 'signature_pad'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedImageService } from './shared-image.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private modalService: NgbModal,private sanitizer :DomSanitizer, private service:SharedImageService) {}
   private codeImage:string 
  ngOnInit(): void {
    this.service.currentMessage.subscribe(data => {
        this.codeImage=data
    })
  }
  open() {
    const modalRef = this.modalService.open(NgbdModalContent);
  }
  transform(){
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.codeImage);
  }
}


@Component({
  selector: 'ngbd-modal-content',
  template: `<div class="modal-header">
                <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="signature-pad">
             <div class="signature-pad--body">
              <canvas #sPad width="900" height="600" style="touch-action: none;"></canvas>
              </div>
            </div>
            <div class="modal-footer">
              <div class="row">
                <div class="col-4">
                  <button class="btn btn-block btn-danger" (click)="clear()">Clear</button>
                </div>
                <div class="col-4">
                <button class="btn btn-block btn-success" (click)="savePNG();activeModal.close('Close click')">PNG</button>
                </div>
                <div class="col-4">
                <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
                </div>
              </div>
            </div>`
})
export class NgbdModalContent implements AfterViewInit {

  
   @ViewChild('sPad', { static: true }) signaturePadElement;
  signaturePad: any;

  constructor(public activeModal: NgbActiveModal, private service:SharedImageService) {}

  ngAfterViewInit(): void {
    this.signaturePad = new SignaturePad(this.signaturePadElement.nativeElement);
  }

  clear() {
    this.signaturePad.clear()
  }

  savePNG() {
    if (this.signaturePad.isEmpty()) {
      alert('Please provide a signature first.');
    } else {
       let base64Image = this.signaturePad.toDataURL(); 
        this.service.changeMessage(base64Image)
        this.download(base64Image, 'signature.png');
    }
  }

  download(dataURL, filename) {
    if (navigator.userAgent.indexOf('Safari') > -1 && navigator.userAgent.indexOf('Chrome') === -1) {
      window.open(dataURL);
    } else {
      const blob = this.dataURLToBlob(dataURL);
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      window.URL.revokeObjectURL(url);
    }
  }

  dataURLToBlob(dataURL) {
    // Code taken from https://github.com/ebidel/filer.js
    const parts = dataURL.split(';base64,');
    const contentType = parts[0].split(':')[1];
    const raw = window.atob(parts[1]);
    const rawLength = raw.length;
    const uInt8Array = new Uint8Array(rawLength);
    for (let i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }
    return new Blob([uInt8Array], { type: contentType });
  }

}
