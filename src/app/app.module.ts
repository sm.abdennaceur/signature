import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent, NgbdModalContent } from './app.component';

import { TruncatePipe } from './truncate.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedImageService } from './shared-image.service'
@NgModule({
  declarations: [
    AppComponent,
    NgbdModalContent,
    TruncatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule  
    
  ],
  providers: [SharedImageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
